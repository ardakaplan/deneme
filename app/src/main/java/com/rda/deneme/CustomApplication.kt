package com.rda.deneme

import android.app.Application
import com.ardakaplan.rdalogger.RDALoggerConfig

/**
 * Created by Arda Kaplan at 2.02.2022 - 15:17
 *
 * ardakaplan101@gmail.com
 */
class CustomApplication :Application(){

    override fun onCreate() {
        super.onCreate()

        RDALoggerConfig.setup(getString(R.string.app_name)).enableLogging(true)
    }
}